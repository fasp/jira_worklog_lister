import os, json, requests, getpass, csv

server_base_URL=''
project=''
        
def create_task(server_base_url, user, password, project, itam_ID, task_summary):
    resource_name = "issue"
    complete_url = "%s/rest/api/latest/%s/" % (server_base_url, resource_name)
    try:
        data = {
            "fields": {
                "project": {
                    "key": project
                },
                "issuetype": {
                    "name": "New Feature"
                },
                "summary": task_summary+' - '+itam_ID,
                "fixVersions": [
                    {"id": "53688"}
                ],
                # team TA.LE
                "customfield_10613": {
                    "id": "12023"
                },
                "assignee": {
                    "name": user
                }
            }
        }
        response = requests.post(complete_url, auth=(user, password), headers = {'Content-Type' : 'application/json'}, data=json.dumps(data))
    except Exception as ex:
        print("EXCEPTION: %s " % ex.msg)
        return None
    if response.status_code / 100 != 2:
        print("ERROR: status %s" % response.status_code)
        return None
    issue = json.loads(response.body_string())
    return issue

s=requests.Session()
username=os.getlogin()
password=getpass.getpass('Password for user '+username+':')
with open('tasks.csv') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';')
    for row in reader:
        print(row['ITAM'], row['summary'])
        create_task(server_base_URL, username, password, project, row['ITAM'], row['summary'])