#!/usr/bin/python

import requests
import getpass
from datetime import datetime, timedelta
import logging
logging.basicConfig(format='%(message)s')
baseURL='! enter jira server here !'
username='! Enter user ID here !'
issue_hours={}  # TODO make set

ITAM_ID='customfield_10002'

class TK_Issue(object):
    _issueID=None
    _log=None
    _summary=None
    _auftrag_id=None

    def log(self):
        return self._log

    def summary(self):
        return self._summary
    
    def auftragID(self):
        return self._auftrag_id
        
    def __init__(self, issueID, auftrag_id, to_be_logged, summary):
        self._issueID=issueID
        self._auftrag_id=auftrag_id
        self._log=to_be_logged
        self._summary=summary
        
def get_issues_by_user(username):    
    request='search?jql=assignee='+username
    r = requests.get(baseURL + request, auth=(username, password))
    if r.status_code==200:
        content=r.json()
        for issue in content['issues']:
            #log_issue_details(issue)
            get_details_for_issue(issue)
    elif r.status_code==401:
        logging.error('Falsches Passwort oder Benutzer')
    else:
        logging.error('Ein Fehler ist aufgetreten. Schade.')

def log_issue_details(issue):
    progress=issue['fields']['progress']
    hours_spent=progress['progress']/60**2
    estimated=progress['total']/60**2
    percent=progress['percent'] if 'percent' in progress else 0        
    print(issue['key']+': '+ issue['fields']['summary'], '\n\tGearbeitet '+str(round(hours_spent, 2))+' von '+str(round(estimated, 2))+' ('+str(percent)+'%)')
        
def get_details_for_issue(issue):
    issueID=issue['key']
    summary=issue['fields']['summary']
    itam_id=issue['fields'][ITAM_ID]
    request='issue/'+issueID+'/worklog'
    r = requests.get(baseURL + request, auth=(username, password))
    content=r.json()
    for log in content['worklogs']:
        logdate=(datetime.strptime(log['started'][:10], '%Y-%m-%d')).date()
        if date_is_in_current_week(logdate):
            already_logged=issue_hours[issueID].log() if issueID in issue_hours else 0
            new_log=log['timeSpentSeconds']/60**2
            to_be_logged=already_logged+new_log
            issue_hours[issueID]= TK_Issue(issueID, itam_id, to_be_logged, summary)

def date_is_in_current_week(date):
    today=datetime.now().date()
    start = today - timedelta(days=today.weekday())
    end = start + timedelta(days=6)
    return start<=date<=end

password=getpass.getpass('Password:')
s=requests.Session()
s.auth=(username, password)
get_issues_by_user(username)
for issue in issue_hours:
    tk_issue=issue_hours[issue]
    print('Auftrag '+tk_issue.auftragID()+' ('+issue+'): '+str(issue_hours[issue].log())+' Stunden\n\t'+issue_hours[issue].summary()+'\n')